/*
 * Copyright (c) 2019 Andrew A. Tasso <andrew@tasso.dev>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package dev.tasso.aout2.chapter1.example1;

public class SimpleParserTests {

	public static void main(String[] args) {
		try {
			SimpleParserTests.testReturnsZeroWhenEmptyString();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testReturnsZeroWhenEmptyString() {

		//use Java's reflection API to get the current
		//           method's name
		// it's possible to hard code this,
		//but it’s a useful technique to know
		String testName = new Object() {}.getClass().getEnclosingMethod().getName();

		try {

			SimpleParser p = new SimpleParser();
			int result = p.parseAndSum("");

			if(result != 0) {

				//Calling the helper method
				TestUtil.showProblem(testName, "Parse and sum should have returned 0 on an empty string");

			}
		}
		catch (Exception e) {
			TestUtil.showProblem(testName, e.toString());
			e.printStackTrace();
		}
	}
}
