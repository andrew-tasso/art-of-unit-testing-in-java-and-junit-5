# The Art of Unit Testing in Java and JUnit 5

An adaptation of the examples from The Art of Unit Testing 2nd ed. using Java and JUnit 5

## Repository Structure

This repository is divided by chapter, each containing examples that correlate to listings in the book. Since each 
listing in the book is not a complete source set, the examples contained in this repository are comprised of multiple 
listings. Each of the example sub-projects is a standalone project with a complete source set.

## Contents

### Chapter 1

#### Example 1.1 - Simple Parser Test

##### Listing(s)
* 1.1, 1.2, 1.3

##### Notable Differences
* The `else` statement in [`SimpleParser.parseAndSum()`](Chapter-1/Example-1.1-Simple-Parser/src/main/java/dev/tasso/aout2/chapter1/example1/SimpleParser.java#L23) throws an [`IllegalArgumentException`](https://docs.oracle.com/javase/8/docs/api/java/lang/IllegalArgumentException.html) (the closest approximation of .NET's [`InvalidOperationException`](https://docs.microsoft.com/en-us/dotnet/api/system.invalidoperationexception)).
* All usages of .NET's [`Exception.ToString()`](https://docs.microsoft.com/en-us/dotnet/api/system.exception.tostring) are replaced with Java's [`Throwable.printStackTrace()`](https://docs.oracle.com/javase/8/docs/api/java/lang/Throwable.html#printStackTrace--).
* Java lacks [verbatim string literals](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/verbatim). String concatenation and line-break characters are used within [`SimpleParserTests.testReturnsZeroWhenEmptyString()`](Chapter-1/Example-1.1-Simple-Parser/src/main/java/dev/tasso/aout2/chapter1/example1/SimpleParserTests.java#L34) instead.

#### Example 1.2 - More Generic showProblem() Method

##### Listing(s)
* 1.1, 1.3, 1.4

##### Notable Differences
* The `else` statement in [`SimpleParser.parseAndSum()`](Chapter-1/Example-1.2-More-Generic-Show-Problem-Method/src/main/java/dev/tasso/aout2/chapter1/example1/SimpleParser.java#L23) throws an [`IllegalArgumentException`](https://docs.oracle.com/javase/8/docs/api/java/lang/IllegalArgumentException.html) (the closest approximation of .NET's [`InvalidOperationException`](https://docs.microsoft.com/en-us/dotnet/api/system.invalidoperationexception)).
* The usage of .NET's [`MethodBase.GetCurrentMethod()`](https://docs.microsoft.com/en-us/dotnet/api/system.reflection.methodbase.getcurrentmethod) within `SimpleParserTests.testReturnsZeroWhenEmptyString()` is replaced with the usage of [reflection and an anonymous inner class](https://stackoverflow.com/a/5891326) to determine which method it is defined in.
* All usages of .NET's [`Exception.ToString()`](https://docs.microsoft.com/en-us/dotnet/api/system.exception.tostring) are replaced with Java's [`Throwable.printStackTrace()`](https://docs.oracle.com/javase/8/docs/api/java/lang/Throwable.html#printStackTrace--).
* Java lacks [verbatim string literals](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/verbatim). String concatenation and line-break characters are used within [`TestUtil.showProblem()`](Chapter-1/Example-1.2-More-Generic-Show-Problem-Method/src/main/java/dev/tasso/aout2/chapter1/example1/TestUtil.java#L5) instead.

## Credits
 
* Andrew A. Tasso (@andrew.tasso andrew@tasso.dev)
* Roy Osherove (https://osherove.com/)
    * The Art of Unit Testing: with Examples in C#. Manning, 2014. (http://artofunittesting.com)
 
## License
 
The MIT License (MIT)

Copyright (c) 2019 Andrew A. Tasso <andrew@tasso.dev>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
